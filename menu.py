import tkinter as tk
import matplotlib.pyplot as plt
import numpy as np

from abc import ABC, abstractmethod
from point import Point, TraceurBuilder
from mpl_toolkits.basemap import Basemap


class Menu:
    windown = None
    create_point = None
    listbox = None

    point1 = None
    point2 = None

    check_loxodromie = None
    check_orthodromie = None
    label_point1 = None
    label_point2 = None

    points = [Point(48.8, 2.3, 'Paris'), Point(23.6, -102.5, 'Mexico'), Point(35.78, 139.7, 'Tokyo'),
              Point(-33.9, 18.4, 'Le Cap'), Point(-53.1, -70.9, 'Punta Arenas'), Point(-83.6, 102.5, 'Test')]

    def __init__(self):
        self.windown = tk.Tk()
        self.check_loxodromie = tk.IntVar()
        self.check_orthodromie = tk.IntVar()

        self.create_point = ComponentCreatePoint(self)
        self.listbox = ComponentListBox(self)

    def draw(self):
        self.windown.title('Navigation aérienne')
        self.windown.minsize(720, 480)
        self.windown.config(background="#798081")
        self.__draw_buttons()
        self.__draw_information()
        self.create_point.draw()
        self.listbox.draw()
        self.windown.mainloop()

    def __draw_buttons(self):
        but1 = tk.Button(self.windown, text="Dessiner la carte", bg="white", fg="black", command=self.__draw_map)
        but1.place(relwidth=0.97, x=10, y=440)

        but1 = tk.Button(self.windown, text="Remettre à 0", bg="white", fg="black", command=self.__clear_points)
        but1.place(width=185, x=525, y=220)

    def __draw_map(self):
        if self.point1 is not None and self.point2 is not None:

            print("Point1 - long: " + str(self.point1.long) + ", lat: " + str(self.point1.lat))
            print("Point2 - long: " + str(self.point2.long) + ", lat: " + str(self.point2.lat))

            m = Basemap()
            builder = TraceurBuilder(self.point1, self.point2).draw_points()

            if self.check_loxodromie.get() == 1:
                builder.draw_loxodromie()

            if self.check_orthodromie.get() == 1:
                builder.draw_orthodromie()

            m.shadedrelief()

            m.drawparallels(np.linspace(-90, 90, 24))
            m.drawmeridians(np.linspace(-180, 180, 60))
            plt.show()

    def __clear_points(self):
        self.point1 = None
        self.point2 = None
        self.label_point1.configure(text="Point1: Aucun")
        self.label_point2.configure(text="Point2: Aucun")

    def __draw_information(self):
        c1 = tk.Checkbutton(self.windown, text="Loxodromie", variable=self.check_loxodromie, font={"Lato", 10},
                            bg="#798081", activebackground="#798081")
        c1.place(x=720 / 3, y=400)

        c2 = tk.Checkbutton(self.windown, text="Orthodromie", variable=self.check_orthodromie, font={"Lato", 10},
                            bg="#798081", activebackground="#798081")
        c2.place(x=720 / 1.5, y=400)

        self.label_point1 = tk.Label(self.windown, text="Point1: Aucun", font={"Lato", 10}, bg="#798081")
        self.label_point1.place(x=720 / 3, y=380)
        self.label_point2 = tk.Label(self.windown, text="Point2: Aucun", font={"Lato", 10}, bg="#798081")
        self.label_point2.place(x=720 / 1.5, y=380)


class Component(ABC):
    menu = None

    def __init__(self, menu):
        self.menu = menu

    @abstractmethod
    def draw(self):
        pass


class ComponentCreatePoint(Component):

    def __init__(self, menu):
        super().__init__(menu)

    entry1 = None
    entry2 = None

    def draw(self):
        label1 = tk.Label(self.menu.windown, text="Longitude:", font={"Lato", 10}, bg="#798081")
        label1.place(x=525, y=18)
        self.entry1 = tk.Entry(self.menu.windown, width=30)
        self.entry1.place(x=525, y=50)

        label2 = tk.Label(self.menu.windown, text="Latitude:", font={"Lato", 10}, bg="#798081")
        label2.place(x=525, y=98)
        self.entry2 = tk.Entry(self.menu.windown, width=30)
        self.entry2.place(x=525, y=130)

        but1 = tk.Button(self.menu.windown, text="Créer le point", bg="white", fg="black", command=self.__create_point)
        but1.place(width=185, x=525, y=180)

    def __create_point(self):
        if self.entry1.get() != "" and self.entry2.get() != "":
            try:
                long = float(self.entry1.get())
                lat = float(self.entry2.get())

                if long > 180 or long < -180 or lat > 90 or lat < -90:
                    print("Longitude ou latitude invalide !")
                else:
                    self.menu.points.append(Point(lat, long))
                    self.menu.listbox.draw()
            except ValueError:
                print("Longitude ou latitude invalide !")


class ComponentListBox(Component):

    def __init__(self, menu):
        super().__init__(menu)

    lb = None

    def draw(self):
        label = tk.Label(self.menu.windown, text="Liste des points", font={"Lato", 15}, bg="#798081")
        label.place(x=10, y=15)

        self.lb = tk.Listbox(self.menu.windown, width=30, height=22)
        for i in range(0, len(self.menu.points)):
            point = self.menu.points[i]
            self.lb.insert(i + 1, self.get_point_name(point, i))

        self.lb.bind("<<ListboxSelect>>", self.__select_listbox_event)
        self.lb.place(x=10, y=50)

    def __select_listbox_event(self, event):
        sel = self.lb.selection_get()
        point = None
        index = -1

        for i in range(0, len(self.menu.points)):
            p = self.menu.points[i]
            if self.get_point_name(p, i) == sel:
                point = p
                index = i

        if point is None or index == -1:
            print("Erreur point corompu !")
            return None

        if point == self.menu.point1:
            return None

        if self.menu.point1 is None:
            self.menu.point1 = point
            self.menu.label_point1.configure(text="Point1: " + self.get_point_name(point, index))
        else:
            self.menu.point2 = point
            self.menu.label_point2.configure(text="Point2: " + self.get_point_name(point, index))

    @staticmethod
    def get_point_name(point, index):
        return point.nom if point.nom is not None else "Point " + str(index)

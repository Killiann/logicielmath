from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import math as ma


class Point:
    lat = 0.0
    long = 0.0
    latRad = 0.0
    longRad = 0.0
    nom = ''

    def __init__(self, lat, long, nom=None, radian=False):
        self.nom = nom
        if radian:
            self.lat = ma.degrees(lat)
            self.long = ma.degrees(long)
            self.latRad = lat
            self.longRad = long
        else:
            self.lat = lat
            self.long = long
            self.latRad = ma.radians(lat)
            self.longRad = ma.radians(long)

    def draw(self):
        plt.gca().add_patch(plt.Circle((self.long, self.lat), 3, color='black'))
        if self.nom is not None:
            plt.text(self.long, self.lat, ' ' + self.nom, fontsize=11)

    def translate(self, long):
        self.long += long
        self.longRad += ma.radians(long)


class TraceurBuilder:
    point1 = None
    point2 = None

    def __init__(self, point1, point2):
        # On fait en sorte que la long du point1 soit tjr plus petite que celle du point2 pour simplifier le code
        if point1.long < point2.long:
            self.point1 = point1
            self.point2 = point2
        else:
            self.point1 = point2
            self.point2 = point1
        plt.plot([-180, 180], [0, 0], 'black') # équateur

    def draw_points(self):
        self.point1.draw()
        self.point2.draw()
        return self

    def draw_loxodromie(self):
        TraceurLoxodromie(self.point1, self.point2).draw()
        return self

    def draw_orthodromie(self):
        TraceurOrthodromie(self.point1, self.point2).draw()
        return self


class Traceur(ABC):
    point1 = None
    point2 = None
    COLOR = ''

    @abstractmethod
    def __init__(self, point1, point2, COLOR=''):
        self.COLOR = COLOR
        self.point1 = point1
        self.point2 = point2

    @abstractmethod
    def draw(self):
        pass


class TraceurLoxodromie(Traceur):

    def __init__(self, point1, point2):
        super().__init__(point1, point2, 'blue')

    def draw(self):
        if abs(self.point1.long - self.point2.long) <= 180:
            plt.plot([self.point1.long, self.point2.long], [self.point1.lat, self.point2.lat], self.COLOR)
        else:
            plt.plot([self.point1.long, self.point2.long - 360], [self.point1.lat, self.point2.lat], self.COLOR)
            plt.plot([self.point1.long + 360, self.point2.long], [self.point1.lat, self.point2.lat], self.COLOR)


class TraceurOrthodromie(Traceur):
    Q = ma.pi / (1.852 * 60 * 180)
    l = 100
    other_side = False

    def __init__(self, point1, point2):
        super().__init__(point1, point2, 'red')

    def draw(self):
        print("Distance: " + str(round(self.__calculate_distance()*6371, 0)) + " km")

        timer = 50000
        while round(self.__calculate_distance(), 1) > 0:
            if timer == 0:
                break
            new_point = self.calculate_new_point()
            plt.plot([self.point1.long, new_point.long], [self.point1.lat, new_point.lat], self.COLOR)

            if new_point.long < -180:
                self.other_side = True
                new_point.translate(360)

            self.point1 = new_point
            timer -= 1

        # Pour fix un bug ou le trait n'arrive pas rellement au point2
        plt.plot([self.point1.long, self.point2.long], [self.point1.lat, self.point2.lat], self.COLOR)

    def calculate_new_point(self):
        cap = self.__calculate_cap()
        if cap > 1:
            cap = 1
        if cap < -1:
            cap = -1
        alpha = ma.acos(cap) if abs(self.point1.long - self.point2.long) <= 180 and not self.other_side \
            else 2 * ma.pi - ma.acos(cap)

        return Point(self.point1.latRad + ma.cos(alpha) * self.l * self.Q,
                     self.point1.longRad + ((ma.sin(alpha) / ma.cos(self.point1.latRad)) * self.l * self.Q),
                     "M'", True)

    def __calculate_cap(self):
        dis = self.__calculate_distance()
        return (ma.sin(self.point2.latRad) - ma.sin(self.point1.latRad) * ma.cos(dis)) / \
               (ma.cos(self.point1.latRad) * ma.sin(dis))

    def __calculate_distance(self):
        return ma.acos(ma.sin(self.point1.latRad) * ma.sin(self.point2.latRad) + ma.cos(self.point1.latRad)
                       * ma.cos(self.point2.latRad) * ma.cos(self.point1.longRad - self.point2.longRad))
